### PGF Demo

This demo will run the needed containers to test [PGF](https://gitlab.com/ontic/ericsson-pgf-server)
functionality:
* PGF Server at `http://localhost:8080/PGF_Server`
* PGF Mitigation Plan Simulator at `http://localhost:8008`
* Analytics Simulator at `http://localhost:8080/AnalyticsNodeSimulator`
* PGF Database based on Postgres 9.3

As it is a docker-compose file, you must be on the folder in order to run
the demo with the following commands. If you want to use other file name 
or other path, specify with `-f /another-folder/another-compose`

#Run
```bash
./start-pgf-demo.sh
```

# Stop and clean database
```bash
docker-compose stop
docker-compose rm
```
