### Use Case 3 demo

# Run
```bash
docker-compose up -d
```

Start 3 different VLC on the host, connecting to the urls:
* Gold user: http://localhost:9000
* Silver user: http://localhost:9001
* Bronze user: http://localhost:9002

When the videos are setup, create a degradation:
```bash
./start-degradation.sh
```

At this point, the kafka may had troubles to correctly setup, 
so make sure if its working and reset the container if needed:
```bash
docker logs uc3demo_analytics_1
```
If there are errors waiting for kafka, run this command:
```bash
docker restart uc3demo_kakfa_1
```

After a few minutes, the degradation should be applied, check `http://localhost:8080/PGF_Server`


To stop policies in order to give back all the customers full bandwidth:
./stop-policies.sh
